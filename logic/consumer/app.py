# An extremely simple EventBridge handler.

import os

import boto3

# Environment configuration
email_source = os.getenv('EMAIL_SOURCE', '')
email_destination = os.getenv('EMAIL_DESTINATION', '')
email_region = os.getenv('EMAIL_REGION', '')

send_email = email_region and email_region not in ['-']
if send_email:
    # An SES (email) client
    ses = boto3.client('ses', region_name=email_region)


def _print(msg: str) -> None:
    """A logging function.
    Simply prints the message prefixing with '## '
    """
    print('## {}'.format(msg))


def lambda_handler(event, context):
    """Sample Lambda function reacting to API calls
    """
    # Log the in-bound event
    _print('event={}'.format(event))
    _print('context={}'.format(context))

    _print('email_source={}'.format(email_source))
    _print('email_destination={}'.format(email_destination))
    _print('email_region={}'.format(email_region))

    # Send an email if we have a source (sender)
    # and a destination (that's not 'SetMe')
    if send_email:
        _print('Sending email...')
        destination = {'ToAddresses': [email_destination]}
        message = {'Subject': {'Data': 'Serverless Demo Email',
                               'Charset': 'UTF-8'},
                   'Body': {'Text': {'Data': 'Hello from the serverless lambda function!',
                                     'Charset': 'UTF-8'}}}
        response = ses.send_email(Source=email_source,
                                  Destination=destination,
                                  Message=message)
        _print('response={}'.format(response))
    else:
        _print('Skipping email (No source, destination or region)')
