# An extremely simple REST endpoint.
# This code handles events sent to the Rest Endpoint.

import datetime
import json

import boto3

# Create CloudWatchEvents client (an EventBridge Bus)
cloudwatch_events = boto3.client('events')

_SOURCE: str = 'com.matildapeak.serverless.demo'
_EVENT_BUS_NAME: str = 'DemoBus'
_DETAIL_TYPE: str = 'Observation'


def _print(msg: str) -> None:
    """A logging function.
    Simply prints the message prefixing with '## '
    """
    print('## {}'.format(msg))


def lambda_handler(event, context):
    """Sample Lambda function reacting to API calls
    """
    # Log the in-bound event
    _print('event={}'.format(event))
    _print('context={}'.format(context))

    # Put (and log) a new event
    response = cloudwatch_events.put_events(
        Entries=[
            {
                # Event envelope fields
                'Source': _SOURCE,
                'EventBusName': _EVENT_BUS_NAME,
                'DetailType': _DETAIL_TYPE,
                'Time': str(datetime.datetime.now()),

                # The main event Body
                'Detail': json.dumps({'key1': 'value1',
                                      'key2': 'value2'})
            }
        ]
    )
    if response['Entries']:
        _print('EventId: {}'.format(response['Entries'][0]['EventId']))
    else:
        _print('(No Event Generated)')

    # Return success to the caller...
    return {'body': 'Hello World!',
            'statusCode': 200}
