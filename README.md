# A simple API-driven EventBridge example
This project contains source code and supporting files for a simple
serverless application that you can deploy with the SAM CLI.
It includes the following files and folders.

-   `logic` - Code for the application's Lambda functions.
-   `template.yaml` - A template that defines the application's AWS resources.

This application reacts to external API HTTPS calls made using AWS IAM
authentication - only authenticated messages pass through the API and
invoke the lambda function. A *Receiver* handles messages sent to the REST
endpoint and generates events, written to the project's EventBridge bus,
that are picked up by a *Consumer*, which then sends an email though
what is expected to be an authenticated *SES* domain.

The application uses several AWS resources, including a **REST API**,
**Usage Plan**, **Lambda functions**  and an **EventBridge bus**.
These resources are defined in the `template.yaml` file in this project.

>   Based loosely on the EventBridge [producer-consumer example].

## Deploy the sample application
The Serverless Application Model Command Line Interface (SAM CLI) is a
extension of the AWS CLI that adds functionality for building and testing
Lambda applications. It uses Docker to run your functions in an Amazon Linux
environment that matches Lambda.

To use the SAM CLI, you need the following tools.

*   [Docker]
*   [Python 3]
*   [SAM CLI]
*   [AWS CLI]

To build and deploy your application for the first time,
run the following in your shell:

    $ sam validate
    $ sam build --use-container
    $ sam deploy --stack-name DemoStack --guided

The first command will build the source of your application.
The second command will package and deploy your application to AWS,
with a series of prompts:

*   **Stack Name**: The name of the stack to deploy to CloudFormation.
    This should be unique to your account and region, and a good starting
    point would be something matching your project name.
*   **AWS Region**: The AWS region you want to deploy your app to.
*   **Parameter**: Provide alternatives for default parameters
*   **Confirm changes before deploy**: If set to yes, any change sets will be
    shown to you before execution for manual review. If set to no, the AWS
    SAM CLI will automatically deploy application changes.
*   **Allow SAM CLI IAM role creation**: Many AWS SAM templates, including
    this example, create AWS IAM roles required for the AWS Lambda function(s)
    included to access AWS services. By default, these are scoped down to
    minimum required permissions. To deploy an AWS CloudFormation stack which
    creates or modified IAM roles, the `CAPABILITY_IAM` value for
    `capabilities` must be provided. If permission isn't provided through this
    prompt, to deploy this example you must explicitly pass
    `--capabilities CAPABILITY_IAM` to the `sam deploy` command.
*   **Save arguments to samconfig.toml**: If set to yes, your choices will be
    saved to a configuration file inside the project, so that in the future
    you can just re-run `sam deploy` without parameters to deploy changes
    to your application.

## Triggering the application (Postman)
As the application provides an API available outside of the serverless
environment you can trigger the initial lambda function by issuing a GET to
the REST API endpoint using [AWS_IAM] authentication.

The endpoint is printed after the application is deployed using the
template's `Outputs` mechanism: -

    ---------------------------------------------------------------------------
    Outputs                                                                                                                                                                                                                                                                
    ---------------------------------------------------------------------------
    Key            RestApi                                                                                                                                                                                                                                            
    Description    Demo API URL                                                                                                                                                                                                                                       
    Value          https://??.execute-api.us-east-1.amazonaws.com/Demo/observation                                                                                                                                                                                        
    ---------------------------------------------------------------------------

Use the URL that's revealed in a [Postman] GET request. As the API requires
authentication using an IAM account simply invoking the REST endpoint
will result in an error: -

    {
      "message": "Missing Authentication Token"
    }

This is good - our code execution (which incurs a cost) is protected from
execution because the API call needs an IAM account (encoded in the message header)
as part of the authentication.

In Postman click the **Authentication** tab for the request, select **Type**
of **AWS Signature** and enter a suitable ``AccessKey`` and ``SecretKey``
and ``AWS Region``.

When this request is made it should be successful and you should see: -

    Hello World!

## Lambda function logs
To simplify troubleshooting, SAM CLI has a command called `sam logs`.
`sam logs` lets you fetch logs generated by your deployed Lambda function
from the command line. In addition to printing the logs on the terminal,
this command has several nifty features to help you quickly find the bug.

>   NOTE: This command works for all AWS Lambda functions;
    not just the ones you deploy using SAM.

    $ sam logs -n <function>
    
>   You may need to add `--region <region>` if your default region is not the
    one you deployed to.

And you'll see live statistics on the function's execution as the endpoint is
invoked:

    [...] START RequestId: 8bc6cd16-400e-4d55-a406-5f19d140a6b9 Version: $LATEST
    [...] END RequestId: 8bc6cd16-400e-4d55-a406-5f19d140a6b9
    [...] REPORT RequestId: 8bc6cd16-400e-4d55-a406-5f19d140a6b9	Duration: 1.31 ms	Billed Duration: 100 ms	Memory Size: 128 MB	Max Memory Used: 51 MB	

You can find more information and examples about filtering Lambda function
logs in the [SAM CLI Documentation].

## Cleanup
To delete the sample application that you created, use the AWS CLI.
Assuming you used your project name for the stack name, you can run the
following:

    $ aws cloudformation delete-stack --stack-name DemoStack

>   You may need to add `--region <region>` if your default region is not the
    one you deployed to.

## Resources
See the [AWS SAM developer guide] for an introduction to SAM specification,
the SAM CLI, and serverless application concepts.

Next, you can use AWS Serverless Application Repository to deploy ready to
use Apps that go beyond hello world samples and learn how authors developed
their applications: [AWS Serverless Application Repository main page].

---

[aws cli]: https://pypi.org/project/awscli/
[aws cloudformation]: https://docs.aws.amazon.com/AWSCloudFormation/latest/UserGuide/aws-template-resource-type-ref.html
[aws iam]: https://www.youtube.com/watch?v=KXyATZctkmQ
[aws sam developer guide]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/what-is-sam.html
[aws serverless application repository main page]: https://aws.amazon.com/serverless/serverlessrepo/
[docker]: https://hub.docker.com/search/?type=edition&offering=community
[postman]: https://www.postman.com
[python 3]: https://www.python.org/downloads/
[sam cli]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-install.html
[sam cli documentation]: https://docs.aws.amazon.com/serverless-application-model/latest/developerguide/serverless-sam-cli-logging.html
[the sam specification]: https://github.com/awslabs/serverless-application-model/blob/master/versions/2016-10-31.md
[producer-consumer example]: https://github.com/aws-samples/amazon-eventbridge-producer-consumer-example
